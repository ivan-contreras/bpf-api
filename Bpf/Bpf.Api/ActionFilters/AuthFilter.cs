﻿using Bpf.Api.Global;
using Bpf.Api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Bpf.Api.ActionFilters
{
    public class AuthFilter : AuthorizeAttribute, IAsyncAuthorizationFilter
    {
        public RolesEnum[] AuthorizedRoles { get; set; }

        async Task IAsyncAuthorizationFilter.OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            try
            {
                var contextUser = context.HttpContext.Request.HttpContext.User;
                var userId = int.Parse(contextUser.Claims.Where(c => c.Type == Constants.UserIdClaim).FirstOrDefault().Value);
                
                var _userService = (UserService)context.HttpContext.RequestServices.GetService(typeof(UserService));
                var userServiceResponse = await _userService.GetByIdAsync(userId);

                if (userServiceResponse.Result == null || userServiceResponse.HasError)
                {
                    context.Result = new BadRequestResult();
                }
                else
                {
                    var user = userServiceResponse.Result;
                    var roleServiceResponse = await _userService.GetRoleIdAsync(user);

                    if (roleServiceResponse.HasError)
                    {
                        context.Result = new UnauthorizedResult();
                    }
                    else
                    {
                        if (AuthorizedRoles != null && !AuthorizedRoles.Contains((RolesEnum)roleServiceResponse.Result))
                        {
                            context.Result = new ForbidResult();
                        }
                    }
                }
            }
            catch (Exception)
            {
                context.Result = new BadRequestResult();
            }
        }
    }
}
