﻿namespace Bpf.Api.Models
{
    public class SelectItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
