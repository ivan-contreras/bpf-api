﻿using AutoMapper;
using Bpf.Api.Global;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Bpf.Api.Controllers
{
    public class ApiResponse<T>
    {
        public T Result { get; set; }
        public bool HasError { get; set; }
        public string Message { get; set; }

        public ApiResponse()
        {
            HasError = false;
        }
    }

    public class BaseController : ControllerBase
    {
        protected readonly IMapper _mapper;

        public int GetAuthenticatedUserId
        {
            get
            {
                var user = Request.HttpContext.User;
                var id = user.Claims.Where(c => c.Type == Constants.UserIdClaim).FirstOrDefault().Value;
                return int.Parse(id);
            }
        }

        public BaseController(IMapper mapper)
        {
            _mapper = mapper;
        }

        protected ApiResponse<object> GetApiErrorResponse()
        {
            return new ApiResponse<object>
            {
                Message = Constants.ApiErroMessage,
                HasError = true
            };
        }
    }
}
