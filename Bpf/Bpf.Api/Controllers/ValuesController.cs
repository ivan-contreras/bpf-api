﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bpf.Api.ActionFilters;
using Bpf.Api.Entities;
using Bpf.Api.Global;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Bpf.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public ValuesController(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            //await _roleManager.CreateAsync(new ApplicationRole
            //{
            //    Name = "Super Admin",
            //    NormalizedName = "Super Admin"
            //});
            //await _roleManager.CreateAsync(new ApplicationRole
            //{
            //    Name = "Administrative",
            //    NormalizedName = "Administrative"
            //});
            //await _roleManager.CreateAsync(new ApplicationRole
            //{
            //    Name = "Payroll",
            //    NormalizedName = "Payroll"
            //});
            
            //var applicationUser = new ApplicationUser
            //{
            //    UserName = "Ivan",
            //    Email = "icontreras@enchufate.pe",
            //    FullName = "Ivan"
            //};

            //var result = await _userManager.CreateAsync(applicationUser, "ivan");
            //await _userManager.AddToRoleAsync(applicationUser, "Super Admin");

            return new string[] { "value1", "value2" };
        }

        [HttpGet("ForAll")]
        // [Authorize]
        [AuthFilter]
        //GET : /api/UserProfile
        public string GetUserProfile()
        {
            return "Web method for user with jwt";
        }

        [HttpGet("ForAdmin")]
        [AuthFilter(AuthorizedRoles = new RolesEnum[] { RolesEnum.SuperAdmin })]
        public string GetForAdmin()
        {
            return "Web method for Admin";
        }

        [HttpGet("ForCustomer")]
        [AuthFilter(AuthorizedRoles = new RolesEnum[] { RolesEnum.Payroll })]
        public string GetForPayroll()
        {
            return "Web method for Payroll";
        }

        [HttpGet("ForAdminOrPayroll")]
        // [Authorize(Roles = "Super Admin,Payroll")]
        [AuthFilter(AuthorizedRoles = new RolesEnum[] { RolesEnum.SuperAdmin, RolesEnum.Payroll })]
        public string GetForAdminOrPayroll()
        {
            return "Web method for Admin or Payroll";
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        [Authorize]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
