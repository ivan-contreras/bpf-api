﻿using AutoMapper;
using Bpf.Api.ActionFilters;
using Bpf.Api.Entities;
using Bpf.Api.Global;
using Bpf.Api.Models;
using Bpf.Api.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bpf.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : BaseController
    {
        private readonly UserService _userService;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserController(UserService userService,
            UserManager<ApplicationUser> userManager,
            IMapper mapper) : base(mapper)
        {
            _userService = userService;
            _userManager = userManager;
        }
        
        [HttpGet("{id}")]
        [AuthFilter(AuthorizedRoles = new RolesEnum[] { RolesEnum.SuperAdmin })]
        public async Task<IActionResult> GetById([FromRoute]int id)
        {
            var serviceResponse = await _userService.GetByIdAsync(id);
            
            if (serviceResponse.HasError)
            {
                return Ok(GetApiErrorResponse());
            }

            var apiResponse = new ApiResponse<UserModel>
            {
                Result = _mapper.Map<UserModel>(serviceResponse.Result)
            };

            return Ok(apiResponse);
        }
        
        [HttpGet("GetByFilter")]
        [AuthFilter(AuthorizedRoles = new RolesEnum[] { RolesEnum.SuperAdmin })]
        public async Task<IActionResult> GetByFilter([FromQuery] string filter,
            [FromQuery] string sortOrder,
            [FromQuery] string sortColumn,
            [FromQuery] int pageNumber,
            [FromQuery] int pageSize)
        {
            var serviceResponse = await _userService.GetByFilter(filter, sortOrder, sortColumn, pageNumber, pageSize);

            if (serviceResponse.HasError)
            {
                return Ok(GetApiErrorResponse());
            }

            var apiResponse = new ApiResponse<object>
            {
                Result = serviceResponse.Result
            };

            return Ok(apiResponse);
        }

        [HttpGet("GetSelectItemsByFilter/{filter}")]
        [AuthFilter(AuthorizedRoles = new RolesEnum[] { RolesEnum.SuperAdmin })]
        public async Task<IActionResult> GetSelectItemsByFilter([FromRoute]string filter)
        {
            var serviceResponse = await _userService.GetSelectItemsByFilter(filter);

            if (serviceResponse.HasError)
            {
                return Ok(GetApiErrorResponse());
            }

            var apiResponse = new ApiResponse<List<SelectItem>>
            {
                Result = serviceResponse.Result
            };

            return Ok(apiResponse);
        }
    }
}
