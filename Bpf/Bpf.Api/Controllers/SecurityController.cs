﻿using AutoMapper;
using Bpf.Api.ActionFilters;
using Bpf.Api.Entities;
using Bpf.Api.Models;
using Bpf.Api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Bpf.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SecurityController : BaseController
    {
        private readonly UserService _userService;
        private readonly UserManager<ApplicationUser> _userManager;

        public SecurityController(UserService userService,
            UserManager<ApplicationUser> userManager,
            IMapper mapper) : base(mapper)
        {
            _userService = userService;
            _userManager = userManager;
        }

        [HttpPost("Login"), AllowAnonymous]
        public async Task<IActionResult> Login([FromBody]LoginModel model)
        {
            var user = await _userManager.FindByNameAsync(model.UserName);

            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                // var roles = await _userManager.GetRolesAsync(user);
                var serviceResponse = _userService.CreateJwt(user.Id.ToString());

                if (serviceResponse.HasError)
                {
                    return Ok(GetApiErrorResponse());
                }

                var apiResponse = new ApiResponse<AccountModel>
                {
                    Result = _mapper.Map<AccountModel>(user)
                };
                apiResponse.Result.Token = serviceResponse.Result;

                return Ok(apiResponse);
            }
            else
            {
                return Ok(new ApiResponse<object>
                {
                    Message = "Username or password is incorrect.",
                    HasError = true
                });
            }
        }

        [AuthFilter]
        [HttpGet("AmIAuthenticated")]
        public IActionResult AmIAuthenticated()
        {
            var apiResponse = new ApiResponse<bool>
            {
                Result = true
            };

            return Ok(apiResponse);
        }

        [AuthFilter]
        [HttpGet("GetMyInfo")]
        public async Task<IActionResult> GetMyInfo()
        {
            var serviceResponse = await _userService.GetByIdAsync(GetAuthenticatedUserId);

            if (serviceResponse.HasError)
            {
                return Ok(GetApiErrorResponse());
            }

            var apiResponse = new ApiResponse<AccountModel>
            {
                Result = _mapper.Map<AccountModel>(serviceResponse.Result)
            };

            return Ok(apiResponse);
        }
    }
}
