USE [Bpf]
GO
/****** Object:  StoredProcedure [dbo].[Users.GetByFilter]    Script Date: 7/3/2019 9:27:22 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Users.GetByFilter]
(
	@Filter nvarchar(100) = '',
	@SortOrder nvarchar(10),
	@SortColumn nvarchar(50) = 'id',
	@PageNumber int,
	@PageSize int
)
AS
BEGIN

SELECT COUNT(1) AS Total 
FROM AspNetUsers x
WHERE 
	x.Id like '%' + @Filter + '%' OR
	x.FullName like '%' + @Filter + '%'

SELECT x.Id, x.FullName
FROM AspNetUsers AS x
WHERE 
	x.Id like '%' + @Filter + '%' OR
	x.FullName like '%' + @Filter + '%'
ORDER BY
	CASE WHEN @SortColumn = 'id' AND @SortOrder = 'desc' THEN x.Id END DESC,    
    CASE WHEN @SortColumn = 'id' AND @SortOrder = 'asc' THEN x.Id END ASC,    
    CASE WHEN @SortColumn = 'fullName' AND @SortOrder = 'desc' THEN x.FullName END DESC,
    CASE WHEN @SortColumn = 'fullName' AND @SortOrder = 'asc' THEN x.FullName END ASC
OFFSET @PageNumber * @PageSize ROWS 
FETCH NEXT @PageSize ROWS ONLY

END
