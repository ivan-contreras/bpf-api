﻿using Bpf.Api.Global;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Bpf.Api.Infrastructure
{
    public class Jwt
    {
        private readonly IConfiguration _configuration;

        public Jwt(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string GenerateJwt(string identifier)
        {
            // var options = new IdentityOptions();

            var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, identifier),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(Constants.UserIdClaim, identifier),
                    // new Claim(options.ClaimsIdentity.RoleClaimType, roles.FirstOrDefault())
                };

            var jwtSecretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Tokens:JwtSecretKey"]));
            var signingCredentials = new SigningCredentials(jwtSecretKey, SecurityAlgorithms.HmacSha256);

            // Create the JWT security token and encode it.
            var jwt = new JwtSecurityToken
            (
                issuer: _configuration["Tokens:Issuer"],
                audience: _configuration["Tokens:Issuer"],
                claims: claims,
                notBefore: DateTime.Now,
                expires: DateTime.UtcNow.Add(TimeSpan.FromDays(1)),
                signingCredentials: signingCredentials
            );

            var token = new JwtSecurityTokenHandler().WriteToken(jwt);

            return token;
        }
    }
}
