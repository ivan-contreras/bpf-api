﻿using AutoMapper;
using Bpf.Api.Entities;
using Bpf.Api.Models;

namespace Bpf.Api.Infrastructure
{
    public class AutoMapping
    {
        public static MapperConfiguration Configure()
        {
            return new MapperConfiguration(config =>
            {
                config.AddProfile(new AutoMappingProfile());
            });
        }

        public class AutoMappingProfile : Profile
        {
            public AutoMappingProfile()
            {
                //Models from Entities mappings
                CreateMap<ApplicationUser, UserModel>()
                    //.ForMember(m => m.Id, obj => obj.MapFrom(e => e.Id))
                    //.ForMember(m => m.FullName, obj => obj.MapFrom(e => e.FullName))
                    .ReverseMap();

                CreateMap<ApplicationUser, AccountModel>()
                    //.ForMember(m => m.UserName, obj => obj.MapFrom(e => e.UserName))
                    //.ForMember(m => m.Email, obj => obj.MapFrom(e => e.Email))
                    //.ForMember(m => m.FullName, obj => obj.MapFrom(e => e.FullName))
                    .ReverseMap();
            }
        }
    }
}
