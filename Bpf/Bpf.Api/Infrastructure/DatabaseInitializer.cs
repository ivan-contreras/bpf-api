﻿using Bpf.Api.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace Bpf.Api.Infrastructure
{
    public class DatabaseInitializer
    {
        public static async Task Initialize(IServiceProvider serviceProvider, string testUserPassword = "test")
        {
            using (var context = new DatabaseContext(serviceProvider.GetRequiredService<DbContextOptions<DatabaseContext>>()))
            {
                // For sample purposes seed both with the same password.
                // Password is set with the following:
                // dotnet user-secrets set SeedUserPW <pw>
                // The admin user can do anything

                await CreateRoles(serviceProvider);
                await CreateUsers(serviceProvider, testUserPassword);

                //SeedDB(context, adminId);
            }
        }

        public static void SeedDB(DatabaseContext context, string adminID)
        {
            // context.Database.EnsureCreated();
            

            //context.Contact.AddRange(
            //    new Contact
            //    {
            //        Name = "Debra Garcia",
            //        Address = "1234 Main St",
            //        City = "Redmond",
            //        State = "WA",
            //        Zip = "10999",
            //        Email = "debra@example.com",
            //        Status = ContactStatus.Approved,
            //        OwnerID = adminID
            //    });
         }

        private static async Task CreateUsers(IServiceProvider serviceProvider, string testUserPassword)
        {
            var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();

            var user1 = new ApplicationUser
            {
                UserName = "ivan.contreras",
                Email = "ivan.contreras@proximitycr.com",
                FullName = "Ivan Contreras"
            };

            await userManager.CreateAsync(user1, testUserPassword);
            await userManager.AddToRoleAsync(user1, "Super Admin");

            var user2 = new ApplicationUser
            {
                UserName = "fabricio.duarte",
                Email = "fabricio.duarte@proximitycr.com",
                FullName = "Fabricio Duarte"
            };

            await userManager.CreateAsync(user2, testUserPassword);
            await userManager.AddToRoleAsync(user2, "Super Admin");
        }

        private static async Task CreateRoles(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetService<RoleManager<ApplicationRole>>();

            await roleManager.CreateAsync(new ApplicationRole
            {
                Name = "Super Admin",
                NormalizedName = "Super Admin"
            });

            await roleManager.CreateAsync(new ApplicationRole
            {
                Name = "Administrative",
                NormalizedName = "Administrative"
            });

            await roleManager.CreateAsync(new ApplicationRole
            {
                Name = "Payroll",
                NormalizedName = "Payroll"
            });

            //var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();

            //var user = await userManager.FindByIdAsync(uid);

            //if (user == null)
            //{
            //    throw new Exception("The testUserPw password was probably not strong enough!");
            //}

            //IR = await userManager.AddToRoleAsync(user, role);

            //return IR;
        }

        //public static void Initialize(DatabaseContext context)
        //{
        //    context.Database.EnsureCreated();

        //    if (!context.Roles.Any())
        //    {
        //    }
        //    context.SaveChanges();

        //}
    }
}

