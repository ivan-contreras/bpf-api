﻿namespace Bpf.Api.Global
{
    public static class Constants
    {
        public static readonly string UserIdClaim = "UserId";
        public static readonly string ApiErroMessage = "Error executing the process";
    }
}
