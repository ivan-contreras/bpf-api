﻿namespace Bpf.Api.Global
{
    public enum RolesEnum
    {
        SuperAdmin = 1,
        Administrative = 2,
        Payroll = 3
    }
}
