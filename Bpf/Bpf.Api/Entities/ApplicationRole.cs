﻿using Microsoft.AspNetCore.Identity;

namespace Bpf.Api.Entities
{
    public class ApplicationRole : IdentityRole<int>
    {
    }
}
