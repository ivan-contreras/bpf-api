﻿using Bpf.Api.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Bpf.Api.Services
{
    public class ServiceResponse<T>
    {
        public T Result { get; set; }
        public bool HasError { get; set; }
        public int ErrorCode { get; set; }
        public Exception Exception { get; internal set; }
    }

    public class BaseService
    {
        protected readonly DatabaseContext _context;

        public BaseService(DatabaseContext context)
        {
            _context = context;
        }
        
        protected ServiceResponse<TResult> Execute<TResult>(Func<TResult> func, int errorCode = 0)
        {
            var response = new ServiceResponse<TResult>();

            try
            {
                response.Result = func.Invoke();
                response.ErrorCode = errorCode;
                response.HasError = false;
                response.Exception = null;
            }
            catch (Exception ex)
            {

                response.ErrorCode = errorCode;
                response.Result = default(TResult);
                response.HasError = true;
                response.Exception = ex;
            }

            return response;
        }

        protected async Task<ServiceResponse<TResult>> ExecuteAsync<TResult>(Func<Task<TResult>> func, int errorCode = 0)
        {
            var response = new ServiceResponse<TResult>();

            try
            {
                response.Result = await func.Invoke();
                response.ErrorCode = errorCode;
                response.HasError = false;
                response.Exception = null;
            }
            catch (Exception ex)
            {

                response.ErrorCode = errorCode;
                response.Result = default(TResult);
                response.HasError = true;
                response.Exception = ex;
            }

            return response;
        }

        #region "Stored Procedure Execution Helper"
        
        public async Task<JArray> ExecuteStoredProcedureAsync(Action<DbCommand> cmd)
        {
            var conn = _context.Database.GetDbConnection();   
            var finalResult = new JArray();

            try
            {
                var records = new List<Dictionary<string, object>>();

                var newCmd = conn.CreateCommand();
                newCmd.CommandType = CommandType.StoredProcedure;
                
                await conn.OpenAsync();
                cmd(newCmd);

                using (var reader = await newCmd.ExecuteReaderAsync(CommandBehavior.CloseConnection))
                {
                    while (await reader.ReadAsync())
                    {
                        records.Add(Enumerable.Range(0, reader.FieldCount)
                            .ToDictionary(reader.GetName, reader.GetValue));
                    }
                }

                var camelCaseSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };
                var jsonStr = JsonConvert.SerializeObject(records, camelCaseSettings);
                finalResult = JArray.Parse(jsonStr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                    conn.Close();
            }

            return finalResult;
        }

        public async Task<object> ExecutePagedStoredProcedureAsync(Action<DbCommand> cmd)
        {
            var conn = _context.Database.GetDbConnection();
            object finalResult = null;

            try
            {
                var totalRecords = 0;
                var records = new List<Dictionary<string, object>>();

                var newCmd = conn.CreateCommand();
                newCmd.CommandType = CommandType.StoredProcedure;
                
                await conn.OpenAsync();
                cmd(newCmd);

                using (var reader = await newCmd.ExecuteReaderAsync(CommandBehavior.CloseConnection))
                {
                    totalRecords = await reader.ReadAsync() ? reader.GetInt32(0) : -1;

                    await reader.NextResultAsync();

                    while (await reader.ReadAsync())
                    {
                        records.Add(Enumerable.Range(0, reader.FieldCount)
                            .ToDictionary(reader.GetName, reader.GetValue));
                    }
                }
                
                var camelCaseSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };
                var jsonStr = JsonConvert.SerializeObject(records, camelCaseSettings);
                var jsonArray = JArray.Parse(jsonStr);

                finalResult = new { totalRecords, pageRecords = jsonArray };
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                    conn.Close();
            }

            return finalResult;
        }

        public List<Dictionary<string, object>> ExecuteStoredProcedure(Action<DbCommand> cmd)
        {
            var conn = _context.Database.GetDbConnection();
            var result = new List<Dictionary<string, object>>();

            try
            {
                var newCmd = conn.CreateCommand();
                newCmd.CommandType = CommandType.StoredProcedure;

                conn.Open();
                cmd(newCmd);
                
                using (var reader = newCmd.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    while (reader.Read())
                    {
                        result.Add(Enumerable.Range(0, reader.FieldCount)
                            .ToDictionary(reader.GetName, reader.GetValue));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (conn != null && conn.State == ConnectionState.Open)
                    conn.Close();
            }

            return result;
        }

        public void SetParameterCommand(DbCommand cmd, string name, object value)
        {
            var p = cmd.CreateParameter();
            p.ParameterName = name;
            p.Value = value;
            cmd.Parameters.Add(p);
        }

        // To test
        public List<T> MapToList<T>(DbDataReader dr)
        {
            List<T> list = new List<T>();
            T obj = default(T);

            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();
                foreach (PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (!Equals(dr[prop.Name], DBNull.Value))
                    {
                        prop.SetValue(obj, dr[prop.Name], null);
                    }
                }

                list.Add(obj);
            }

            return list;
        }

        #endregion

        //public async Task<List<Dictionary<string, object>>> ExecuteStoredProcedureAsync(Action<DbCommand> cmd)
        //{
        //    var conn = _context.Database.GetDbConnection();
        //    var result = new List<Dictionary<string, object>>();

        //    try
        //    {
        //        var newCmd = conn.CreateCommand();
        //        newCmd.CommandType = CommandType.StoredProcedure;

        //        await conn.OpenAsync();
        //        cmd(newCmd);

        //        using (var reader = await newCmd.ExecuteReaderAsync(CommandBehavior.CloseConnection))
        //        {
        //            while (await reader.ReadAsync())
        //            {
        //                result.Add(Enumerable.Range(0, reader.FieldCount)
        //                    .ToDictionary(reader.GetName, reader.GetValue));
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        if (conn != null && conn.State == ConnectionState.Open)
        //            conn.Close();
        //    }

        //    return result;
        //}
    }
}
