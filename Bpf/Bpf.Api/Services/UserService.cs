﻿using Bpf.Api.Entities;
using Bpf.Api.Infrastructure;
using Bpf.Api.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bpf.Api.Services
{
    public class UserService : BaseService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly Jwt _jwtService;

        public UserService(UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            Jwt jwtService,
            DatabaseContext context) : base(context)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _jwtService = jwtService;
        }

        public ServiceResponse<string> CreateJwt(string userId)
        {
            string Func()
            {
                return _jwtService.GenerateJwt(userId);
            };

            return Execute(Func);
        }
        
        public async Task<ServiceResponse<object>> GetByFilter(string filter, string sortOrder, string sortColumn, int pageNumber, int pageSize)
        {
            async Task<object> FuncAsync()
            {
                return await ExecutePagedStoredProcedureAsync(cmd => {
                    SetParameterCommand(cmd, "Filter", filter);
                    SetParameterCommand(cmd, "SortOrder", sortOrder);
                    SetParameterCommand(cmd, "SortColumn", sortColumn);
                    SetParameterCommand(cmd, "PageNumber", pageNumber);
                    SetParameterCommand(cmd, "PageSize", pageSize);

                    cmd.CommandText = "[Users.GetByFilter]";
                });
            };
            return await ExecuteAsync(FuncAsync);
        }

        public async Task<ServiceResponse<List<SelectItem>>> GetSelectItemsByFilter(string filter)
        {
            async Task<List<SelectItem>> FuncAsync()
            {
                return await _context.Users.Where(x => x.FullName.ToLower().Contains(filter.ToLower())).OrderBy(x => x.Id)
                    .Select(x => new SelectItem
                    {
                        Id = x.Id.ToString(),
                        Name = x.FullName
                    }).ToListAsync();
            };
            return await ExecuteAsync(FuncAsync);
        }

        public async Task<ServiceResponse<ApplicationUser>> GetByIdAsync(int id)
        {
            async Task<ApplicationUser> FuncAsync()
            {
                return await _userManager.FindByIdAsync(id.ToString());
            };
            return await ExecuteAsync(FuncAsync);
        }

        public async Task<ServiceResponse<int>> GetRoleIdAsync(ApplicationUser user)
        {
            async Task<int> FuncAsync()
            {
                var roles = await _userManager.GetRolesAsync(user);
                var roleName = roles.FirstOrDefault();
                var role = await _roleManager.FindByNameAsync(roleName);
                return role.Id;
            };
            return await ExecuteAsync(FuncAsync);
        }
    }
}
